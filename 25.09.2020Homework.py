# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 16:42:52 2020

@author: Xuxin
"""
import pandas as pd
data=pd.read_csv(r"C:\Users\Xuxin\AppData\Local\Temp\Temp1_Data.zip\YoungBank.csv")
print(data["Education"].value_counts())
print(data["Personal_Loan"].value_counts())
#Downsampling
data_majority = data[data.Personal_Loan =='no']
data_minority=data[data.Personal_Loan =='yes']
from sklearn.utils import resample
data_majority_downsampled = resample(data_majority,
n_samples=len(data_minority),
replace=False, random_state=0)
data = pd.concat([data_majority_downsampled,
data_minority])
#encode labeling
from sklearn.preprocessing import LabelEncoder
lb_Edu = LabelEncoder()
data['Education'] = lb_Edu.fit_transform(data['Education'])
print(data["Education"].head())
data.info()
data["Personal_Loan"].value_counts()
#Partition
X=data.iloc[:,data.columns!="Personal_Loan"]
Y=data.iloc[:,-1]
#Normalize the X
X = (X-X.min())/(X.max()-X.min())
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
stratify=Y, test_size=0.25, random_state=0)

#Classification
import sklearn as sk
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix
report = pd.DataFrame(columns=['Model','Acc.Train','Acc.Test'])

# Using k-Nearest Neighbors (kNN)
from sklearn.neighbors import KNeighborsClassifier
knnmodel = KNeighborsClassifier(n_neighbors=6)
knnmodel.fit(X_train, Y_train)
Y_train_pred = knnmodel.predict(X_train)
cmtr = confusion_matrix(Y_train, Y_train_pred)
print("Confusion Matrix Training:\n", cmtr)
acctr_train = accuracy_score(Y_train, Y_train_pred)
print("Accurray Training:", acctr_train)

#test the quality of the classifier
Y_test_pred = knnmodel.predict(X_test)
cmte = confusion_matrix(Y_test, Y_test_pred)
print("Confusion Matrix Testing:\n",cmte)
acctr_test = accuracy_score(Y_test, Y_test_pred)
print("Accurray Testing:", acctr_test)


#Y_test_pred = knnmodel.predict_proba(X_test)

#find the best k
accuracies = []
for k in range(1, 21):
    knnmodel = KNeighborsClassifier(n_neighbors=k)
    knnmodel.fit(X_train, Y_train)
    Y_test_pred = knnmodel.predict(X_test)
    accte = accuracy_score(Y_test, Y_test_pred)
    print(k, accte)
    accuracies.append(accte)
plt.plot(range(1, 21), accuracies)
plt.xlim(1,20)
plt.xticks(range(1, 21))
plt.xlabel('Number of Neighbors')
plt.ylabel('Accuracy')
plt.title('Comparison of Accuracies')

#calculate f1
lb_churn = LabelEncoder()
Y_test_code = lb_churn.fit_transform(Y_test)
Y_test_pred_code = lb_churn.fit_transform(Y_test_pred)
from sklearn.metrics import f1_score
f1te = f1_score(Y_test_code, Y_test_pred_code)
print(f1te)

# test the majority using the model from downplaying data
data2=pd.read_csv(r"C:\Users\Xuxin\AppData\Local\Temp\Temp1_Data.zip\YoungBank.csv")
from sklearn.preprocessing import LabelEncoder
lb_Edu = LabelEncoder()
data2['Education'] = lb_Edu.fit_transform(data2['Education'])
print(data2["Education"].head())
X2=data2.iloc[:,data2.columns!="Personal_Loan"]
Y2=data2.iloc[:,-1]
#Normalize the X2
X2 = (X2-X2.min())/(X2.max()-X2.min())
from sklearn.model_selection import train_test_split
X2_train, X2_test, Y2_train, Y2_test = train_test_split(X2, Y2, test_size=0.25, random_state=0)
Y2_test_pred = knnmodel.predict(X2_test)
cmte = confusion_matrix(Y2_test, Y2_test_pred)
print("Confusion Matrix Testing:\n",cmte)
acctr_test = accuracy_score(Y2_test, Y2_test_pred)
print("Accurray Testing:", acctr_test)
lb_churn = LabelEncoder()
Y2_test_code = lb_churn.fit_transform(Y2_test)
Y2_test_pred_code = lb_churn.fit_transform(Y2_test_pred)
from sklearn.metrics import f1_score
f1te_2 = f1_score(Y2_test_code, Y2_test_pred_code)
print(f1te_2)

#Find the best k 
accuracies2 = []
Fite_3=[]
for k in range(1, 21): 
    knnmodel = KNeighborsClassifier(n_neighbors=k)
    knnmodel.fit(X_train, Y_train)
    Y2_test_pred = knnmodel.predict(X2_test) #Y2 changes according to k
    accte = accuracy_score(Y2_test, Y2_test_pred)#changes according to k
    lb_churn = LabelEncoder()
    Y2_test_code = lb_churn.fit_transform(Y2_test)#change with key
    Y2_test_pred_code = lb_churn.fit_transform(Y2_test_pred)
    f1te= f1_score(Y2_test_code, Y2_test_pred_code)
    Fite_3.append(f1te)
    print(k, accte)
    print(k,f1te)
    print()
    accuracies2.append(accte)
plt.plot(range(1, 21), accuracies2)
plt.plot(range(1, 21), Fite_3)

#Best solution of k is 2,consider the downsampling, the best key is 13!!